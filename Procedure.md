AmyPro data loading
=

## Adding new entries

Data needs to be appended to the "this.entries" array in scripts/controllers.js

Example:
```
{
                "protein_name": "RIP3",
                "ISA_amyloid_name": "",
                "prion_domain": 0,
                "class_name": "functional amyloid",
                "uniprot_id": "Q9Y572",
                "sequence": "SSDSMAQPPQTPETSTFRNQMPSPTSTGTPSPGPRGNQGAERQGMNWSCRTPEPNPVTGRPLVNIYNCSGVQVGDNNYLTMQQTTALPTWGLAPSGKGRGLQHPPPVGSQEGPKDPEAWSRPQGWYNHSGK",
                "pdbs": {
                    "fibrillar": ["5ZCK"],
                    "precursor": [],
                    "soluble": {
                        "sol_wildt": [],
                        "sol_segment": [],
                        "sol_mutant": [],
                        "sol_complex": []
                    }
                },
                "mutations": [],
                "wild_type_amyloidogenic": 1,
                "regions": [
                    {
                        "region_end": "75",
                        "region_sequence": "GVQVGD",
                        "references": ["22817896"],
                        "region_start": "70"
                    }
                ],
                "uniprot_end": "518",
                "references": [
                    "22817896",
                    "29681455"
                ],
                "uniprot_start": "388",
                "parent_protein": "",
                "parent_protein_names": [],
                "species": "Homo sapiens",
                "entry_id": "AP00162",
                "alternative_names": ["Receptor-interacting serine/threonine-protein kinase 3", "RIP-like protein kinase 3", "Receptor-interacting protein 3", "RIP-3", "RIPK3", "RIP3"],
                "experimental_techniques": ["FTIR", "CD", "Congo red binding", "ThT fluorescence", "X-ray fiber diffraction", "negative staining electron microscopy"],
                "uniprot_description": "Essential for necroptosis, a programmed cell death process in response to death-inducing TNF-alpha family members. Upon induction of necrosis, RIPK3 interacts with, and phosphorylates RIPK1 and MLKL to form a necrosis-inducing complex. RIPK3 binds to and enhances the activity of three metabolic enzymes: GLUL, GLUD1, and PYGL. These metabolic enzymes may eventually stimulate the tricarboxylic acid cycle and oxidative phosphorylation, which could result in enhanced ROS production.",
                "description": "RIP1 and RIP3 kinases are central players in TNF-induced programmed necrosis. The RIP homotypic interaction motifs (RHIMs) of RIP1 and RIP3 mediate the assembly of heterodimeric filamentous structures. The fibrils exhibit classical characteristics of β-amyloids."
            }
```

## List of things to create

* JSON file with AmyPro regions keyed on UniProt ID
* Image of the representative PDB structure
* Single JSON file
* Single TXT file
* Single FASTA file
* Update the statistics

## Creating the single files

Single files are created by running utils.py

## Update the footer

Change the date of the last update to the current date