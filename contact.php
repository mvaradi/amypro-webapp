<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Thank you!</title>
    <meta name="description" content="Thank you for your message!">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="styles/main.css">
    <link rel="icon" href="amypro.ico">
</head>
<body>

<?php
$name = $_POST["contact_name"];
$address = $_POST["contact_email"];
$message = $_POST["contact_message"];
$message = wordwrap($message, 70, "\r\n");

$email = "AmyPro contact message from: " . $name . "\n\r";
$email .= "Reply address: " . $address . "\n\r";
$email .= "\n\rMessage: " . $message;

$rita = "Rita.Pancsa@vub.ac.be";
$misi = "neuropa.42@gmail.com";
$to = $rita . ", " . $misi;

mail($to, 'AmyPro : Contact message from ' . $name, $email);
?>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h3 class="alert alert-success">Thank you for your message!</h3>
			<a class="btn btn-default" href="http://www.amypro.net"><span class="glyphicon glyphicon-home"></span> Back to AmyPro</a>

			<h4>You wrote:</h4>
			<p><?php echo $message; ?></p>
		</div>
	</div>
</div>

</body>
</html>


