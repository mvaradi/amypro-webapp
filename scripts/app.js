'use strict';

angular
  .module('amyPro', [
    'ngResource',
    'ui.router',
    'pdb.component.library'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      // route for the home page
      .state('app', {
          url:'/',
          views: {
              'header': {
                  templateUrl : 'views/header.html',
              },
              'content': {
                  templateUrl : 'views/welcome.html',
                  controller: 'WelcomeCtrl'
              },
              'footer': {
                  templateUrl : 'views/footer.html',
              }
          }
      })
      .state('app.browse', {
          url:'browse',
          views: {
              'header@': {
                  templateUrl : 'views/header.html',
              },
              'content@': {
                  templateUrl : 'views/browse.html',
                  controller: 'SearchAndBrowseCtrl'
              },
              'footer': {
                  templateUrl : 'views/footer.html',
              }
          }
      })
      .state('app.search', {
          url:'search',
          views: {
              'header@': {
                  templateUrl : 'views/header.html',
              },
              'content@': {
                  templateUrl : 'views/search.html',
                  controller: 'SearchAndBrowseCtrl'
              },
              'footer': {
                  templateUrl : 'views/footer.html',
              }
          }
      })
      .state('app.entries', {
          url:'entries/:entry_id',
          views: {
              'header': {
                  templateUrl : 'views/header.html',
              },
              'content@': {
                  templateUrl : 'views/entry.html',
                  controller: 'EntryCtrl'
              },
              'footer': {
                  templateUrl : 'views/footer.html',
              }
          },
          reload: true
      })
      .state('app.entry', {
          url:'raw/:entry_list',
          views: {
              'header@': {
                  template: ''
              },
              'content@': {
                  templateUrl : 'views/raw.html',
                  controller: 'RawCtrl'
              },
              'footer@': {
                  template: ''
              }
          }
      })
      .state('app.submit', {
          url:'submit',
          views: {
              'header@': {
                  templateUrl : 'views/header.html',
              },
              'content@': {
                  templateUrl : 'views/submit.html',
                  controller: 'SubmitCtrl'
              },
              'footer@': {
                  templateUrl : 'views/footer.html',
              }
          }
      })
      .state('app.stats', {
          url:'stats',
          views: {
              'header@': {
                  templateUrl : 'views/header.html',
              },
              'content@': {
                  templateUrl: "views/stats.html",
              },
              'footer': {
                  templateUrl : 'views/footer.html',
              }
          }
      })
      .state('app.help', {
          url:'help',
          views: {
              'header@': {
                  templateUrl : 'views/header.html',
              },
              'content@': {
                  templateUrl: "views/help.html",
              },
              'footer': {
                  templateUrl : 'views/footer.html',
              }
          }
      });

    $urlRouterProvider.otherwise('/');
  });
