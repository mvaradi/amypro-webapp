'use strict';

angular.module('amyPro')

	.controller('WelcomeCtrl', ['$scope', 'WelcomeTextService', 'EntryService', function($scope, WelcomeTextService, EntryService){
		// Initialize scope variables for welcome texts
		$scope.texts = WelcomeTextService.getWelcomeTexts();
		$scope.number_of_entries = EntryService.getNumberOfEntries();
		$scope.date = new Date();
	}])

	.controller('RawCtrl', ['$scope', '$stateParams', 'EntryService', function($scope, $stateParams, EntryService){
		$scope.entry_list = $stateParams.entry_list.split(',');
		$scope.entries = [];
		for(var i = 0; i < $scope.entry_list.length; i++){
			var current_entry = EntryService.getEntry($scope.entry_list[i]);
			$scope.entries.push(current_entry);
		}
	}])

	.controller('SubmitCtrl', ['$scope', function($scope){
		// Initialize scope variables for submissions
		$scope.protein_name = "";
		$scope.species = "";
		$scope.pdb = "";
		$scope.uniprot = "";
		$scope.uniprot_start = "";
		$scope.uniprot_end = "";
		$scope.pmids = "";
		$scope.category = "";
		$scope.prion_domain = "";
		$scope.regions = "";
		$scope.sequence = "";

		$scope.setClass = function(clicked){
			$scope.category = clicked;
		}

		$scope.setPrion = function(clicked){
			$scope.prion_domain = clicked;
		}

	}])

	.controller('EntryCtrl', ['$scope', '$http', '$stateParams', 'EntryService', 'CommonNamesService',
									function($scope, $http, $stateParams, EntryService, CommonNamesService){
		// Initialize scope variables for entry views
		$scope.message = false;
		$scope.entry = EntryService.getEntry($stateParams.entry_id);
		if(!$scope.entry){
			$scope.message = "No entries found with this AmyPro ID";
		}
		$scope.pubmed_entries = [];
		$scope.got_pubmed = false; // Is there any publication returned from PubMed?
		var pubmedURL = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi";
		$scope.label = "label label-default";
		$scope.category_tooltip= "";
		$scope.sequenceRegions = {};

		// Relies on the NCBI PubMed API to return details of publications
		// such as authors, journal and date
		// If the API returns nothing, then nothing is shown, only the PubMed IDs
		$scope.add_pubmed_entry = function(pubmed_id){
			$scope.got_pubmed = false;
			$http.get(pubmedURL+"?db=pubmed&id="+pubmed_id+"&version=2.0&retmode=json")
			.then(function(response) {
				for(var key in response.data.result){
					if (key !== "uids"){
                        var current_pubmed_entry = {};
                        current_pubmed_entry["pmid"] = pubmed_id;
						current_pubmed_entry["title"] = response.data.result[key].title;
						current_pubmed_entry["pubdate"] = response.data.result[key].pubdate;
						current_pubmed_entry["source"] = response.data.result[key].source;
						var names = "";
						for (var author in response.data.result[key].authors){
							names += response.data.result[key].authors[author].name+"; ";
						}
						current_pubmed_entry["names"] = names;
						$scope.pubmed_entries.push(current_pubmed_entry);
						$scope.got_pubmed = true;
					}
				}
    		})
		};

		// This sets the style of the label next to the title of the entry page
		// based on the category of the entry
		$scope.setLabel = function(category){
			if(category === "pathogenic"){
				$scope.label = "label label-danger";
				$scope.category_tooltip = "In pathogenic amyloids the amyloid state is responsible for known diseases.";
			}
			else if(category === "functional prion"){
				$scope.label = "label label-warning";
				$scope.category_tooltip = "In functional prions the self-propagating prion form confers functional benefits to the organism.";
			}
			else if(category === "functional amyloid"){
				$scope.label = "label label-warning";
				$scope.category_tooltip = "In functional amyloids the amyloid state confers well-described functional benefits to the organism.";
			}
			else if(category === "biologically not relevant"){
				$scope.label = "label label-default";
				$scope.category_tooltip = "Biologically not relevant - The amyloid state was observed under conditions whose biological relevance is questionable.";
			}
			else if(category === "not known"){
				$scope.label = "label label-default";
				$scope.category_tooltip = "Unknown: The biological relevance of the amyloid state is yet to be elucidated.";
			}

		};

		// This is used to flag those residues in the sequence that
		// are within amyloid forming regions
		// The flag is then used to apply styling to these residues
		$scope.createSequenceList = function(sequence, regions){
			// Flag all the residues as "false", i.e. not part of a region
			for(var i = 0; i < sequence.length; i++){
				$scope.sequenceRegions[i] = {
					"residue": sequence[i],
					"flag": false
				};
			}
			// Examine every region, get start and end indices and
			// switch the flag to "true" for every residue that's
			// within a region
			for(var i = 0; i < regions.length; i++){
				var start = regions[i].region_start;
				var end = regions[i].region_end;
				for(var j = 0; j < sequence.length; j++){
					if(j >= start - 1 && j <= end - 1){
						$scope.sequenceRegions[j]["flag"] = true;
					}
				}
			}
		};

		// Maps species name (Latin) to common name, if available
		$scope.getCommonName = function(species){
			var common_name = CommonNamesService.getCommonName(species);
			if(common_name){
				return "("+common_name+")";
			}
		}

        $scope.addIndex = function(index, offset){
            if(offset){
                return parseInt(index) + parseInt(offset) - 1;
            } else {
                return index;
            }
            
        }

		$scope.check_if_need_comma = function(index, length){
			if (index + 1 < length){
				return true;
			} else {
				return false;
			}
		}

		$scope.has_related_pdb = false;
		$scope.representative_fibrillar = false;
		$scope.representative_soluble = false;
		$scope.representative_segment = false;
		if($scope.entry){
			var pdb_cache = $scope.entry["pdbs"]
			$scope.pdb_ids = pdb_cache["fibrillar"]
			.concat(pdb_cache["soluble"]["sol_wildt"])
			.concat(pdb_cache["soluble"]["sol_segment"])
			.concat(pdb_cache["soluble"]["sol_mutant"])
			.concat(pdb_cache["soluble"]["sol_complex"])
			.concat(pdb_cache["precursor"])
			if($scope.pdb_ids.length > 0){
				$scope.has_related_pdb = true;
			}
			if(pdb_cache["soluble"]["sol_wildt"].length > 0){
				$scope.representative_soluble = pdb_cache["soluble"]["sol_wildt"][0]
			}
			if(pdb_cache["fibrillar"].length > 0){
				$scope.representative_fibrillar = pdb_cache["fibrillar"][0]
			}
			if(pdb_cache["soluble"]["sol_wildt"].length <= 0 && pdb_cache["soluble"]["sol_segment"].length > 0){
				$scope.representative_segment = pdb_cache["soluble"]["sol_segment"][0]
			}
		}

	}])

	.controller('SearchAndBrowseCtrl', ['$scope', 'EntryService', function($scope, EntryService){
		// Initialize scope variables for browsing and searching
		// Browse-specific variables
		$scope.browsable_entries = EntryService.getBrowsingData();

		// Search-specific variables
		$scope.searchable_entries = EntryService.getSearchingData(); // Term search
		$scope.searchable_data = EntryService.getSequenceSearchData(); // Sequence search
		$scope.search_term = "";
		$scope.search_species = "";
		$scope.search_category = "";
		$scope.no_search = true;
		$scope.no_match = true;

		// Common variables
		$scope.sorting_option = "entry_id";
		$scope.a2z_entry = true;
		$scope.a2z_protein = false;
		$scope.a2z_category = false;
		$scope.a2z_species = false;
		$scope.custom_entry_list = {};
		$scope.unique_entry_ids_to_download = [];

		// By default the search result table should be empty
		$scope.perform_search = function(){
			if ($scope.search_term == ""){
				$scope.no_search = true;
			} else {
				$scope.no_search = false;
			}
		}

		// Resets the search terms and clears the table
		$scope.resetSearch = function(){
			$scope.search_term = "";
			$scope.search_category = "";
			$scope.search_species = "";
			$scope.no_search = true;
		}

		// Searches for 100% sequence similarity between an input sequence
		// and the amyloid forming regions of the database
		$scope.search_sequence_match = function(){
			$scope.no_match = true;
			$scope.sequence_matches = [];
			if(!$scope.user_sequence){
				$scope.user_sequence = "";
			}
			// Loop through all the regions {} stored in the database
			for (var i=0; i < $scope.searchable_data.length; i++){
				// Loop through all the keys of a region {}
				// The key is in a format is "region_1", etc.
				for(var j=0; j < Object.keys($scope.searchable_data[i]["regions"]).length; j++){
					// This is the actual sequence of the region that
					// needs to be compared to the input sequence
					var region_sequence = $scope.searchable_data[i]["regions"][j]["region_sequence"];

					var user_input_length = $scope.user_sequence.length;
					for (var k=0; k < user_input_length - 6 + 1; k++){
						// console.log($scope.user_sequence[k]);
						var window_of_6 = $scope.user_sequence.slice(k,k+6);
						// console.log(window_of_6);
						var re = new RegExp(window_of_6, 'g');
						if(region_sequence.match(re)){
							$scope.no_match = false;
							$scope.sequence_matches.push({
								"entry_id" : $scope.searchable_data[i]["entry_id"],
								"protein_name" : $scope.searchable_data[i]["protein_name"],
								"region_sequence" : region_sequence
							});
							break;
						}
					}
				}
			}
		}

		// This selects a category for searching
		$scope.selectCategory = function(selectedCategory){
			$scope.search_category = selectedCategory;
        }
        
        $scope.mutantFilter = "";
        $scope.setMutants = function(){
            if($scope.mutantFilter){
                $scope.mutantFilter = "";
            } else {
                $scope.mutantFilter = "mutant";
            }
        }

        $scope.prionFilter = "";
        $scope.setPrions = function(){
            if($scope.prionFilter){
                $scope.prionFilter = "";
            } else {
                $scope.prionFilter = "prion";
            }
        }

		// This resets the list of entries that can be passed
		// to the "raw", unformatted entry view
		$scope.resetSelectedList = function(){
			$scope.unique_entry_ids_to_download = [];
		}

		// The custom entry list is where every entry id that's
		// been clicked by the user are stored and flagged as
		// true (i.e. send it to the raw view) or false (i.e. don't)
		// This checks if an entry is selected or not
		$scope.isEntrySelected = function(entry_id){
			if($scope.custom_entry_list[entry_id]){
				return true;
			} else {
				return false;
			}
		}

		// This is what happens when the user clicks on an entry to add
		// it to the list of entries available for download
		$scope.checkBoxChange = function(entry_id){
			if (entry_id in $scope.custom_entry_list){
				if($scope.custom_entry_list[entry_id]){
					$scope.custom_entry_list[entry_id] = false;
				} else {
					$scope.custom_entry_list[entry_id] = true;
				}

			} else {
				$scope.custom_entry_list[entry_id] = true;
			}
			$scope.makeIdListUnique();
		};

		// This creates the list of unique entry IDs that are currently
		// selected for downloading
		$scope.makeIdListUnique = function(){
			$scope.resetSelectedList();
			for (var key in $scope.custom_entry_list){
				if($scope.custom_entry_list[key] === true){
					$scope.unique_entry_ids_to_download.push(key);
				}
			}
		}

		// These methods below set the sorting glyphicons and the
		// sorting option
		$scope.sortByEntry = function(){
			if($scope.a2z_entry){
				$scope.sorting_option = "-entry_id";
			}
			else{
				$scope.sorting_option = "entry_id";
			}
			$scope.a2z_entry = !$scope.a2z_entry;
		}

		$scope.sortByProtein = function(){
			if($scope.a2z_protein){
				$scope.sorting_option = "-protein_name";
			}
			else{
				$scope.sorting_option = "protein_name";
			}
			$scope.a2z_protein = !$scope.a2z_protein;
		}

		$scope.sortByCategory = function(){
			if($scope.a2z_category){
				$scope.sorting_option = "-class_name";
			}
			else{
				$scope.sorting_option = "class_name";
			}
			$scope.a2z_category = !$scope.a2z_category;
		}

		$scope.sortBySpecies = function(){
			if($scope.a2z_species){
				$scope.sorting_option = "-species";
			}
			else{
				$scope.sorting_option = "species";
			}
			$scope.a2z_species = !$scope.a2z_species;
		}
	}]);
